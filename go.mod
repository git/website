module apiote.xyz/p/website

go 1.18

require (
	apiote.xyz/p/go-dirty v0.0.0-20211218161334-e486e7b5cf43
	github.com/bytesparadise/libasciidoc v0.8.0
	golang.org/x/crypto v0.11.0
	golang.org/x/text v0.11.0
)

require (
	github.com/alecthomas/chroma/v2 v2.8.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dlclark/regexp2 v1.10.0 // indirect
	github.com/go-task/slim-sprig v0.0.0-20230315185526-52ccab3ef572 // indirect
	github.com/google/pprof v0.0.0-20230705174524-200ffdc848b8 // indirect
	github.com/mna/pigeon v1.1.0 // indirect
	github.com/onsi/ginkgo/v2 v2.11.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	golang.org/x/mod v0.12.0 // indirect
	golang.org/x/sys v0.10.0 // indirect
	golang.org/x/tools v0.11.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
