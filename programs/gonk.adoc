= gonk
apiote <me@apiote.xyz>
:toc:

terminal reader for honk (https://humungus.tedunangst.com/r/honk)

== setup

create file `account` in `XDG_CONFIG_HOME/gonk` (`$HOME/.config/gonk`) with three lines

```
username = {username}
password = {password}
server = {server}
```

replace {variables} with what they should be. server should be just host name -- without `http(s)://` in front

== run

run `gonk` to show honks with colours or `gonk -t` to show them without colours.

== contribute

this project is finished; no more functions will be implemented; all feature requests will be ignored.

this project uses The Code of Merit, which is available as CODE_OF_CONDUCT file.

fixes and patches are welcome; please send them to `gonk@git.apiote.xyz` using `git send-email`. they must include a sign-off to certify agreement to https://developercertificate.org/[developer certificate of origin].

all communication—questions, bugs, etc.—should go through the mailing list available at `gonk@git.apiote.xyz`. note that all communication will be made public at https://asgard.apiote.xyz/.

== mirrors

the canonical repository for this project is https://git.apiote.xyz/gonk.git it’s mirrored at https://notabug.org/apiote/gonk

mirrors exist solely for the sake of the code and any additional functions provided by third-party services (including but not limited to issues and pull requests) will not be used and will be ignored.

== license

gonk is adapted from honk examples

```
Copyright (c) 2019 Ted Unangst <tedu@tedunangst.com>
```

GPL applies (see LICENSE)

```
gonk -- honk reader
Copyright (C) 2020 apiote

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
