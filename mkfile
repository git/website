all:V: website

static/style.css: static_raw/style.css
	minify static_raw/style.css >static/style.css

website: argon.go main.go router.go `echo static/* static/favicon/* templates/*`
	CGO_ENABLED=0 go build -ldflags '-extldflags "-static"'
