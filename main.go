package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"path/filepath"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	pass := flag.String("p", "", "password")
	dataDir := flag.String("d", "/usr/share/website", "data directory")
	stateDir := flag.String("s", "/var/lib/website", "data directory")
	flag.Parse()

	if *pass != "" {
		hash, err := GenerateFromPassword(*pass)
		if err != nil {
			fmt.Println(err)
			return
		}
		ioutil.WriteFile(filepath.Join(*stateDir, "password"), []byte(hash), 0600)
		fmt.Println("Password updated")
		return
	}

	route(*dataDir, *stateDir)
}
